#ifndef TESTTREEFUNCS_H
#define TESTTREEFUNCS_H

#include <QObject>
#include <QTest>
#include "testsrunner.h"
#include "../ConvertC++ExpressionToTex/treefuncs.h"

class TestTreeFuncs : public QObject
{
    Q_OBJECT

private slots:
    void testBuildVariable();
    void testBuildNumber();
    void testBuildArithmetic();
    void testBuildCompare();
    void testBuildLogic();
    void testBuildMath();
    void testBuildComplex();

    void testConvertVariable();
    void testConvertNumber();
    void testConvertArithmetic();
    void testConvertDivision();
    void testConvertComparison();
    void testConvertLogic();
    void testConvertPowerRootExp();
    void testConvertAbsoluteRound();
    void testConvertLogarithmTrigonometric();
    void testConvertComplex_block1();
    void testConvertComplex_block2();
    void testConvertComplex_block3();
};

DECLARE_TEST(TestTreeFuncs);
#endif // TESTTREEFUNCS_H
