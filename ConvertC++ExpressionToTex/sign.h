/*!
 *\file sign.h
 *\brief Файл с описанием класса унарного минуса.
 */

#ifndef SIGN_H
#define SIGN_H

#include "unaryoperation.h"
#include <QString>

/*!
 * \class Sign : public UnaryOperation
 * \brief Класс унарного минуса.
 */
class Sign : public UnaryOperation
{
public:
    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // SIGN_H
