/*!
 *\file error.h
 *\brief Файл класса ошибки.
 */

#ifndef ERROR_H
#define ERROR_H

#include <QString>

/*!
 *\class Error
 *\brief Описание ошибки.
 */
class Error
{
public:
    QString message; /**< \brief Сообщение об ошибке. */
    int lexNum; /**< \brief Номер лексемы для сообщения. */
    int code; /**< \brief Код ошибки. */
};

#endif // ERROR_H
