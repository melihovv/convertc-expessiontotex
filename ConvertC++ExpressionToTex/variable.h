/*!
 *\file variable.h
 *\brief Файл c описанием узла переменной.
 */

#ifndef VARIABLE_H
#define VARIABLE_H

#include <QString>
#include "node.h"

/*!
 *\class Variable : public Node
 *\brief Класс, описывающий переменную.
 */
class Variable : public Node
{
public:
    QString name; /**< \brief Имя переменной. */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // VARIABLE_H
