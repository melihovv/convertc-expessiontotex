#include "round.h"

int Round::precedence()
{
    return 9;
}

NodeType Round::nodeType()
{
    return NodeType::ROUND;
}

void Round::convertNodeToTeXExp(QString& result, int precedence)
{
    result += value == "ceil()" ? "\\lceil " : "\\lfloor ";
    child->convertNodeToTeXExp(result, 0);
    result += value == "ceil()" ? "\\rceil " : "\\rfloor ";
}

void Round::clear()
{
    child->clear();
    delete this;
}

bool Round::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Round* r = (Round*) other;
        result = this->value == r->value && this->child->compare(r->child);
    }

    return result;
}
