/*!
 *\file binaryoperation.h
 *\brief Файл c описанием узла бинарной операции.
 */

#ifndef BINARYOPERATION_H
#define BINARYOPERATION_H

#include "node.h"

/*!
 *\class BinaryOperation : public Node
 *\brief Абстрактный класс, описывающий бинарные операции.
 */
class BinaryOperation : public Node
{
public:
    Node* rightChild; /**< \brief Указатель на правый операнд. */
    Node* leftChild; /**< \brief Указатель на левый операнд. */
};

#endif // BINARYOPERATION_H
