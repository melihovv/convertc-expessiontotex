/*!
 *\file number.h
 *\brief Файл c описанием узла числа.
 */

#ifndef NUMBER_H
#define NUMBER_H

#include <QString>
#include "node.h"

/*!
 *\class Number : public Node
 *\brief Класс, описывающий узел-число.
 */
class Number : public Node
{
public:
    QString value; /**< \brief Значение. */
    bool integer; /**< \brief тип числа: целое, дробное. */
    int radix; /**< \brief Форма записи числа: 8, 10 и 16-ричное. */
    bool exponential; /**< \brief Форма записи дробных чисел. */

    Number();
    int precedence() override;
    NodeType nodeType();
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};
#endif // NUMBER_H
