/*!
 *\file not.h
 *\brief Файл, описывающий логические операции и/или.
 */

#ifndef NOT_H
#define NOT_H

#include <QString>
#include "unaryoperation.h"

/*!
 *\class Not : public UnaryOperation
 *\brief Класс логической операции не.
 */
class Not : public UnaryOperation
{
public:
    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // NOT_H
