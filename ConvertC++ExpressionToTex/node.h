/*!
 *\file node.h
 *\brief Файл c описанием классов дерева.
 */

#ifndef NODE_H
#define NODE_H

#include <QString>
#include "nodetype.h"

/*!
 *\class Node
 *\brief Абстрактный класс, содержащий данные об узле дерева выражения.
 */
class Node
{
public:
    /*!
     *\brief Возвращает приоритет данного узла.
     *\return Приоритет узла.
     */
    virtual int precedence() = 0;

    /*!
     *\brief Возвращает тип данного узла.
     *\return Тип узла.
     */
    virtual NodeType nodeType() = 0;

    /*!
     *\brief Переводит данный узел дерева на язык ТеХ.
     *\param[in] precedence Приоритет узла-родителя.
     *\param[in,out] result Строка с получившимся выражением.
     */
    virtual void convertNodeToTeXExp(QString& result, int precedence) = 0;

    /*!
     *\brief Удаление узла.
     */
    virtual void clear() = 0;

    /*!
     *\brief Сравнение узлов.
     */
    virtual bool compare(Node* other) = 0;
};

#endif // NODE_H
