#include "not.h"

int Not::precedence()
{
    return 7;
}

NodeType Not::nodeType()
{
    return NodeType::NOT;
}

void Not::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();

    if (prec < precedence)
    {
        result += "\\left( ";
    }

    result += "\\overline{ ";
    child->convertNodeToTeXExp(result, prec);
    result += "} ";

    if (prec < precedence)
    {
        result += "\\right) ";
    }
}

void Not::clear()
{
    child->clear();
    delete this;
}

bool Not::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();
    
    if (result)
    {
        Not* n = (Not*) other;
        result = this->child->compare(n->child);
    }

    return result;
}
