#include "exponent.h"

int Exponent::precedence()
{
    return 8;
}

NodeType Exponent::nodeType()
{
    return NodeType::EXPONENT;
}

void Exponent::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();
    result += "e^{ ";
    child->convertNodeToTeXExp(result, 0);
    result += "} ";
}

void Exponent::clear()
{
    child->clear();
    delete this;
}

bool Exponent::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Exponent* exp = (Exponent*) other;
        result = this->child->compare(exp->child);
    }

    return result;
}
