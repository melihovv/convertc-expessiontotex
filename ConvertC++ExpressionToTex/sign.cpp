#include "sign.h"

int Sign::precedence()
{
    return 3;
}

NodeType Sign::nodeType()
{
    return NodeType::SIGN;
}

void Sign::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();

    if (prec < precedence)
    {
        result += "\\left( ";
    }

    result += "- ";
    child->convertNodeToTeXExp(result, 5);

    if (prec < precedence)
    {
        result += "\\right) ";
    }
}

void Sign::clear()
{
    child->clear();
    delete this;
}

bool Sign::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Sign* s = (Sign*) other;
        result = this->child->compare(s->child);
    }

    return result;
}
