/*!
 *\file power.h
 *\brief Файл, описывающий класс возведения в степень.
 */

#ifndef POWER_H
#define POWER_H

#include <QString>
#include "binaryoperation.h"
#include "number.h"
#include "divide.h"

/*!
 *\class Power : public BinaryOperation
 *\brief Класс возведения в степень.
 */
class Power : public BinaryOperation
{
public:
    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // POWER_H
