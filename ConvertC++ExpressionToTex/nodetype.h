/*!
 *\file nodetype.h
 *\brief Файл c описанием возможных видов узлов дерева.
 */

#ifndef NODETYPE_H
#define NODETYPE_H

/*!
 *\enum NodeType
 *\brief Виды возможных узлов дерева.
 */
enum class NodeType
{
    NUMBER, VARIABLE, ARITHMETIC, DIVIDE, SIGN, COMPARASION, LOGIC, NOT, POWER,
    ABSOLUTE, ROUND, ROOT, EXPONENT, LOGARITHM, TRIGONOMETRIC
};

#endif // NODETYPE_H
