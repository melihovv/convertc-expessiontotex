#include "root.h"

int Root::precedence()
{
    return 8;
}

NodeType Root::nodeType()
{
    return NodeType::ROOT;
}

void Root::convertNodeToTeXExp(QString& result, int precedence)
{
    result += "\\sqrt";

    if (value == 3)
    {
        result += "[3]";
    }

    result += "{ ";
    child->convertNodeToTeXExp(result, 0);
    result += "} ";
}

void Root::clear()
{
    child->clear();
    delete this;
}

bool Root::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Root* r = (Root*) other;
        result = this->value == r->value && this->child->compare(r->child);
    }

    return result;
}
