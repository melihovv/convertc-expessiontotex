#include "arithmetic.h"

int Arithmetic::precedence()
{
    return value == '*' ? 5 : 4;
}

NodeType Arithmetic::nodeType()
{
    return NodeType::ARITHMETIC;
}

void Arithmetic::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();

    if (prec < precedence)
    {
        result += "\\left( ";
    }

    leftChild->convertNodeToTeXExp(result, prec);

    if (value == '*')
    {
        result += "\\cdot";
    }
    else
    {
        result += value;
    }

    result += ' ';

    rightChild->convertNodeToTeXExp(result, prec + 1);

    if (prec < precedence)
    {
        result += "\\right) ";
    }
}

void Arithmetic::clear()
{
    leftChild->clear();
    rightChild->clear();
    delete this;
}

bool Arithmetic::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Arithmetic* ar = (Arithmetic*) other;
        result =
            this->value == ar->value &&
            this->leftChild->compare(ar->leftChild) &&
            this->rightChild->compare(ar->rightChild);
    }

    return result;
}
