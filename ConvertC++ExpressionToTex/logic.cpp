#include "logic.h"

int Logic::precedence()
{
    return value == "&&" ? 1 : 0;
}

NodeType Logic::nodeType()
{
    return NodeType::LOGIC;
}

void Logic::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();

    if (prec < precedence)
    {
        result += "\\left( ";
    }

    leftChild->convertNodeToTeXExp(result, prec);
    result += value == "&&" ? "\\wedge " : "\\vee ";
    rightChild->convertNodeToTeXExp(result, prec);

    if (prec < precedence)
    {
        result += "\\right) ";
    }
}

void Logic::clear()
{
    leftChild->clear();
    rightChild->clear();
    delete this;
}

bool Logic::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Logic* l = (Logic*) other;
        result =
            this->value == l->value &&
            this->leftChild->compare(l->leftChild) &&
            this->rightChild->compare(l->rightChild);
    }

    return result;
}
