#include "treefuncs.h"

Node* addNodeFromStr(const QStringList& lexems, int& lexNum) throw (Error)
{
    Node* result;
    QString tmp = lexems[lexNum];

    try
    {
        if (tmp == "+" || tmp == "-" || tmp == "*")
        {
            if (lexNum < 2)
            {
                throw lexNum;
            }

            Arithmetic* node = new Arithmetic();
            node->value = tmp[0].toLatin1();
            node->rightChild = addNodeFromStr(lexems, --lexNum);
            node->leftChild = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "/")
        {
            if (lexNum < 2)
            {
                throw lexNum;
            }

            Divide* node = new Divide();
            node->rightChild = addNodeFromStr(lexems, --lexNum);
            node->leftChild = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "--" || tmp == "++")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            // Постфиксные инкремент и декремент пропускаем.
            Node* node = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "(--)" || tmp == "(++)")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Arithmetic* node = new Arithmetic();
            node->value = tmp == "(++)" ? '+' : '-';
            Number* rightChild = new Number();
            rightChild->value = "1";
            rightChild->integer = true;
            rightChild->radix = 10;
            node->rightChild = rightChild;
            node->leftChild = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "(-)")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Sign* node = new Sign();
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == ">" || tmp == ">=" || tmp == "<" ||
            tmp == "<=" || tmp == "==" || tmp == "=" || tmp == "!=")
        {
            if (lexNum < 2)
            {
                throw lexNum;
            }

            Comparison* node = new Comparison();
            node->value = tmp == "==" ? "=" : tmp;
            node->rightChild = addNodeFromStr(lexems, --lexNum);
            node->leftChild = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "&&" || tmp == "||")
        {
            if (lexNum < 2)
            {
                throw lexNum;
            }

            Logic* node = new Logic();
            node->value = tmp;
            node->rightChild = addNodeFromStr(lexems, --lexNum);
            node->leftChild = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "!")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Not* node = new Not();
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "pow()")
        {
            if (lexNum < 2)
            {
                throw lexNum;
            }

            Power* node = new Power();
            node->rightChild = addNodeFromStr(lexems, --lexNum);
            node->leftChild = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "abs()" || tmp == "fabs()")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Absolute* node = new Absolute();
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "sqrt()" || tmp == "cbrt()")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Root* node = new Root();
            node->value = (tmp == "sqrt()") ? 2 : 3;
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "floor()" || tmp == "ceil()")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Round* node = new Round();
            node->value = tmp;
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "exp()")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Exponent* node = new Exponent();
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "log()" || tmp == "log10()")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Logarithm* node = new Logarithm();
            node->value = tmp == "log()" ? 0 : 10;
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        else if (tmp == "acos()" || tmp == "asin()" || tmp == "atan()" ||
            tmp == "cos()" || tmp == "sin()" || tmp == "tan()")
        {
            if (lexNum < 1)
            {
                throw lexNum;
            }

            Trigonometric* node = new Trigonometric();
            node->value = tmp;
            node->child = addNodeFromStr(lexems, --lexNum);
            result = node;
        }
        // Начинается с цифры - значит число.
        else if (tmp[0].isDigit())
        {
            bool OK = true;
            Error err;
            Number* node = new Number();
            node->value = tmp;

            // Если число - 0.
            if (tmp == "0")
            {
                node->integer = true;
                node->radix = 10;
            }
            // Если число содержит точку.
            else if (tmp.contains('.'))
            {
                tmp.toDouble(&OK);

                if (!OK)
                {
                    err.message = "Incorrect fractional number: lexem #";
                }

                node->integer = false;

                // И букву е.
                if (tmp.contains('e'))
                {
                    node->exponential = true;
                }
            }
            // Если начинается с нуля, то это 8-ричная или 16-ричная форма записи.
            else if (tmp[0] == '0')
            {
                node->radix = (tmp.size() > 1 && tmp[1] == 'x') ? 16 : 8;
                tmp.toInt(&OK, node->radix);

                if (!OK)
                {
                    if (node->radix == 8)
                    {
                        err.message = "Incorrect octal notation of number: lexem #";
                    }
                    else if (node->radix == 16)
                    {
                        err.message = "Incorrect hexadecimal notation of number: lexem #";
                    }
                }

                node->integer = true;
            }
            // Содержит букву е, но начинается не с 0х.
            else if (tmp.contains('e'))
            {
                tmp.toDouble(&OK);

                if (!OK)
                {
                    err.message = "Incorrect fractional number: lexem #";
                }

                node->integer = false;
                node->exponential = true;
            }
            // Целое число в десятичной форме записи.
            else
            {
                tmp.toInt(&OK);

                if (!OK)
                {
                    err.message = "Incorrect number: lexem #";
                }

                node->integer = true;
                node->radix = 10;
            }

            if (!OK)
            {
                err.lexNum = lexNum;
                // Код ошибки: ошибка в записи числа.
                err.code = 4;
                throw err;
            }

            result = node;
        }
        // Начинается с буквы - переменная.
        else if (tmp[0].isLetter())
        {
            bool correct = true;
            int size = tmp.size();

            for (int i = 0; i < size && correct; i++)
            {
                // Может содержать только буквы, цифры и символ нижнего подчеркивания.
                if (!(tmp[i].isLetter() || tmp[i].isDigit() || tmp[i] == '_'))
                {
                    correct = false;
                }
            }

            if (!correct)
            {
                Error err;
                err.message = "Invalid symbol in the name of variable: lexem #";
                err.lexNum = lexNum;

                // Код ошибки: неправильное имя переменной.
                err.code = 3;
                throw err;
            }

            Variable* node = new Variable();
            node->name = tmp;
            result = node;
        }
        // Неизвестный тип лексемы.
        else
        {
            Error err;
            err.message = "Invalid operation: lexem #";
            err.lexNum = lexNum;
            // Код ошибки: неизветсная операция.
            err.code = 2;
            throw err;
        }
    }
    // Ловим недостаток операндов.
    catch (int num)
    {
        Error err;
        err.message = "Lack of operands for operation: lexem #";
        err.lexNum = num;
        // Код ошибки: неверное количество операндов.
        err.code = 1;
        throw err;
    }

    return result;
}

Node* buildTree(const QString& str) throw (...)
{
    if (str.length() == 0)
    {
        throw "Void string";
    }

    QStringList lexems = str.split(" ");
    int size = lexems.size();
    Node* root = addNodeFromStr(lexems, --size);

    if (size >= 1)
    {
        Error err;
        err.lexNum = size;
        err.message = "Redundant operands: lexems from 1 to ";
        // Код ошибки: неверное количество операндов.
        err.code = 1;
        throw err;
    }

    return root;
}

QString readExpFromFile(const QString& filename) throw (char*)
{
    QString result;
    QFile file(filename);

    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream input(&file);
        result = input.readAll();
    }
    else
    {
        throw "Error: cannot open file for read";
    }

    return result;
}

void writeExpToFile(const QString& exp, const QString& filename) throw (char*)
{
    QFile file(filename);

    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream output(&file);
        output << exp;
    }
    else
    {
        throw "Error: cannot open file for write";
    }
}
