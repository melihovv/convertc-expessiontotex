#include "absolute.h"

int Absolute::precedence()
{
    return 9;
}

NodeType Absolute::nodeType()
{
    return NodeType::ABSOLUTE;
}

void Absolute::convertNodeToTeXExp(QString& result, int precedence)
{
    result += "| ";
    child->convertNodeToTeXExp(result, 0);
    result += "| ";
}

void Absolute::clear()
{
    child->clear();
    delete this;
}

bool Absolute::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Absolute* abs = (Absolute*) other;
        result = this->child->compare(abs->child);
    }

    return result;
}
