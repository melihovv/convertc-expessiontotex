/*!
 *\file root.h
 *\brief Файл класса корня.
 */

#ifndef ROOT_H
#define ROOT_H

#include "unaryoperation.h"

/*!
 *\class Root : public UnaryOperation
 *\brief Класс корня.
 */
class Root : public UnaryOperation
{
public:
    int value; /**< \brief Степень корня. */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // ROOT_H
