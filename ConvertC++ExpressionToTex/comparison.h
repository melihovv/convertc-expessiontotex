/*!
 *\file comparasion.h
 *\brief Файл, описывающий операции сравнения.
 */

#ifndef COMPARISON_H
#define COMPARISON_H

#include "binaryoperation.h"

/*!
 *\class Comparison : public BinaryOperation
 *\brief Класс, описывающий операции сравнения.
 */
class Comparison : public BinaryOperation
{
public:
    QString value; /**< \brief Значение. */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // COMPARISON_H
