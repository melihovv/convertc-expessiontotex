/*!
 *\file logarithm.h
 *\brief Файл класса логарифма.
 */

#ifndef LOGARITHM_H
#define LOGARITHM_H

#include "unaryoperation.h"

/*!
 *\class Logarithm : public UnaryOperation
 *\brief Класс логарифма.
 */
class Logarithm : public UnaryOperation
{
public:
    int value; /**< \brief основание логарифма (0 - натуральный, 10 - десятичный). */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // LOGARITHM_H
