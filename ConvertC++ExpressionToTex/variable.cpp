#include "variable.h"

int Variable::precedence()
{
    return 10;
}

NodeType Variable::nodeType()
{
    return NodeType::VARIABLE;
}

void Variable::convertNodeToTeXExp(QString& result, int precedence)
{
    int size = name.size();

    for (int i = 0; i < size; i++)
    {
        // Цифры переводим в нижний регистр.
        if (name[i].isDigit())
        {
            result += '_';
        }
        // Перед нижним подчеркиванием нужно поставить '\'.
        else if (name[i] == '_')
        {
            result += '\\';
        }

        result += name[i];
    }

    result += ' ';
}

void Variable::clear()
{
    delete this;
}

bool Variable::compare(Node* other)
{
    bool result = false;
    result = this->nodeType() == other->nodeType();

    if (result)
    {
        Variable* var = (Variable*) other;
        result = this->name == var->name;
    }

    return result;
}
